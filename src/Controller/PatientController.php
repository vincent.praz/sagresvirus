<?php

namespace App\Controller;

use App\Entity\Admission;
use App\Entity\Patient;
use App\Form\AdmissionType;
use App\Form\PatientType;
use App\Repository\PatientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/patient")
 */
class PatientController extends AbstractController {
    /**
     * @Route("/", name="patient_index", methods={"GET"})
     * @param PatientRepository $patientRepository
     * @return Response
     */
    public function index(PatientRepository $patientRepository): Response {
        return $this->render('patient/index.html.twig', [
            'patients' => $patientRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="patient_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response {
        $patient = new Patient();
        $form = $this->createForm(PatientType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($patient);
            $entityManager->flush();

            return $this->redirectToRoute('patient_index');
        }

        return $this->render('patient/new.html.twig', [
            'patient' => $patient,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="patient_show", methods={"GET","POST"})
     * @param Request $request
     * @param Patient $patient
     * @return Response
     */
    public function show(Request $request, Patient $patient): Response {
        $form = $this->createForm(PatientType::class, $patient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('patient_index');
        }

        $admission = new Admission();
        $formAdmission = $this->createForm(AdmissionType::class, $admission);
        $formAdmission->handleRequest($request);

        if ($formAdmission->isSubmitted() && $formAdmission->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($admission);
            $entityManager->flush();

            return $this->redirectToRoute('admission_index');
        }

        return $this->render('patient/show.html.twig', [
            'patient' => $patient,
            'admission' => $admission,
            'form' => $form->createView(),
            'formAdmission' => $formAdmission->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="patient_delete", methods={"DELETE"})
     * @param Request $request
     * @param Patient $patient
     * @return Response
     */
    public function delete(Request $request, Patient $patient): Response {
        if ($this->isCsrfTokenValid('delete' . $patient->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($patient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('patient_index');
    }
}
