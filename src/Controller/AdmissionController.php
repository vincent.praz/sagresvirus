<?php

namespace App\Controller;

use App\Entity\Admission;
use App\Form\AdmissionType;
use App\Repository\AdmissionRepository;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admission")
 */
class AdmissionController extends AbstractController {
    /**
     * @Route("/", name="admission_index", methods={"GET"})
     * @param AdmissionRepository $admissionRepository
     * @return Response
     */
    public function index(AdmissionRepository $admissionRepository): Response {
        return $this->render('admission/index.html.twig', [
            'admissions' => $admissionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/end", name="admission_end")
     * @param $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function end($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $admission = $entityManager->getRepository(Admission::class)->find($id);

        if (!$admission) {
            throw $this->createNotFoundException(
                "No admission found for id " . $id
            );
        }

        $admission->setDeletedAt(new DateTime('now'));
        $entityManager->flush();

        return $this->redirectToRoute('admission_index');
    }
}
