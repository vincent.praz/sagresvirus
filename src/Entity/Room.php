<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 */
class Room {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hospital", inversedBy="rooms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hospital;

    /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="room", orphanRemoval=true)
     */
    private $admissions;

    public function __construct() {
        $this->admissions = new ArrayCollection();
    }

    public function __toString() {
        return $this->getName();
    }


    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getHospital(): ?Hospital {
        return $this->hospital;
    }

    public function setHospital(?Hospital $hospital): self {
        $this->hospital = $hospital;

        return $this;
    }

    public function getCapacity(): ?int {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * @return Collection|Admission[]
     */
    public function getAdmissions(): Collection {
        return $this->admissions;
    }

    public function addAdmission(Admission $admission): self {
        if (!$this->admissions->contains($admission)) {
            $this->admissions[] = $admission;
            $admission->setRoom($this);
        }

        return $this;
    }

    public function removeAdmission(Admission $admission): self {
        if ($this->admissions->contains($admission)) {
            $this->admissions->removeElement($admission);
            // set the owning side to null (unless already changed)
            if ($admission->getRoom() === $this) {
                $admission->setRoom(null);
            }
        }

        return $this;
    }
}
