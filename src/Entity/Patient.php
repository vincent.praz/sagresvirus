<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatientRepository")
 */
class Patient {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $avs;

    /**
     * @ORM\Column(type="date")
     */
    private $birthdate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Admission", mappedBy="patient", orphanRemoval=true)
     */
    private $admissions;

    public function __construct() {
        $this->admissions = new ArrayCollection();
    }

    public function __toString() {
        return $this->getName() . " " . $this->getFirstname();
    }


    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAvs(): ?string {
        return $this->avs;
    }

    public function setAvs(?string $avs): self {
        $this->avs = $avs;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return Collection|Admission[]
     */
    public function getAdmissions(): Collection {
        return $this->admissions;
    }

    public function addAdmission(Admission $admission): self {
        if (!$this->admissions->contains($admission)) {
            $this->admissions[] = $admission;
            $admission->setPatient($this);
        }

        return $this;
    }

    public function removeAdmission(Admission $admission): self {
        if ($this->admissions->contains($admission)) {
            $this->admissions->removeElement($admission);
            // set the owning side to null (unless already changed)
            if ($admission->getPatient() === $this) {
                $admission->setPatient(null);
            }
        }

        return $this;
    }
}
