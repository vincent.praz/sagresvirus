<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdmissionRepository")
 */
class Admission {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Patient", inversedBy="admissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="admissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    public function __construct() {
        $datetimeNow = new \DateTime('now');
        $this->setCreatedAt($datetimeNow);
    }

    public function __toString() {
        return $this->getPatient()."";
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCreatedAt(): ?DateTimeInterface {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDeletedAt(): ?DateTimeInterface {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getPatient(): ?Patient {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self {
        $this->patient = $patient;

        return $this;
    }

    public function getRoom(): ?Room {
        return $this->room;
    }

    public function setRoom(?Room $room): self {
        $this->room = $room;

        return $this;
    }
}
