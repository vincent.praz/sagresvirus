<?php

namespace App\DataFixtures;

use App\Entity\Patient;
use Cassandra\Date;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PatientFixtures extends Fixture {
    public function load(ObjectManager $manager) {
        $patients = [
            ["Praz", "Vincent", "756.1234.5678.97", "04-08-1998"],
            ["Ben Mimoun", "Drissou", "756.1234.5678.01", "01-07-1997"],
            ["Guerreiro", "André", "756.1234.5678.11", "16-11-1996"],
            ["Haefliger", "Sarah", "756.1234.5678.21", "12-11-1989"],
            ["Tokisaki", "Kurumi", "756.1234.5678.31", "04-08-1998"],
            ["Favre", "Clowny", "756.1234.5678.41", "04-08-1997"],
            ["Dupond", "Sebastien", "756.1234.5678.51", "20-08-1996"],
            ["Schmid", "Galien", "756.1234.5678.61", "05-08-1997"],
            ["Faver", "Benoiot", "756.1234.5678.71", "06-07-1997"],
            ["Bee", "Gianni", "756.1234.5678.81", "04-02-1997"],
        ];

        foreach ($patients as $patient) {
            $newPatient = new Patient();

            $newPatient
                ->setName($patient[0])
                ->setFirstname($patient[1])
                ->setAvs($patient[2])
                ->setBirthdate(new DateTime($patient[3]));

            $manager->persist($newPatient);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
