<?php

namespace App\DataFixtures;

use App\Entity\Admission;
use App\Repository\PatientRepository;
use App\Repository\RoomRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AdmissionFixtures extends Fixture implements DependentFixtureInterface {
    private $roomRepository;
    private $patientRepository;

    public function __construct(RoomRepository $roomRepository, PatientRepository $patientRepository) {
        $this->roomRepository = $roomRepository;
        $this->patientRepository = $patientRepository;
    }


    public function load(ObjectManager $manager) {
        $rooms = $this->roomRepository->findAll();
        $patients = $this->patientRepository->findAll();
        foreach ($patients as $patient) {

            shuffle($rooms);
            $admission = new Admission();
            $admission
                ->setCreatedat(new DateTime('now'))
                ->setPatient($patient)
                ->setRoom($rooms[0]);
            $manager->persist($admission);

        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies() {
        return [
            PatientFixtures::class,
            RoomFixtures::class
        ];
    }
}
