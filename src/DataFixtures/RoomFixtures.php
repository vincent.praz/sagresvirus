<?php

namespace App\DataFixtures;

use App\Entity\Room;
use App\Repository\HospitalRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RoomFixtures extends Fixture implements DependentFixtureInterface {
    private $hospitalRepository;

    /**
     * RoomFixtures constructor.
     * @param HospitalRepository $hospitalRepository
     */
    public function __construct(HospitalRepository $hospitalRepository) {
        $this->hospitalRepository = $hospitalRepository;
    }

    public function load(ObjectManager $manager) {
        // $product = new Product();
        // $manager->persist($product);
        $rooms = [
            ["Room 1", 20],
            ["Room 2", 5],
            ["Room 3", 10],
            ["Room 4", 25],
            ["Room 5", 5]
        ];

        $hospitals = $this->hospitalRepository->findAll();
        foreach ($hospitals as $hospital) foreach ($rooms as $room) {
            $newRoom = new Room();
            $newRoom
                ->setName($room[0])
                ->setCapacity($room[1])
                ->setHospital($hospital);
            $manager->persist($newRoom);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getDependencies() {
        return [
            HospitalFixtures::class
        ];
    }
}
