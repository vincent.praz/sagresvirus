<?php

namespace App\DataFixtures;

use App\Entity\Hospital;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class HospitalFixtures extends Fixture {
    public function load(ObjectManager $manager) {
        $hospitals = [
            ["Marmottant", "17 Rue d'Armaillé", "75017", "Paris", "FR"],
            ["Beau-Séjour", "26 Avenue de beau-séjour", "1206", "Genève", "CH"],
            ["Prangins", "3 Chemin Oscar Forel", "1197", "Prangins", "CH"],
            ["Malévoz", "10 Route de Morgins", "1870", "Monthey", "CH"],
            ["HUG", "4 Rue Gabrielle-Perret-Gentil", "1205", "Genève", "CH"]
        ];
        foreach ($hospitals as $hospital) {
            $newHospital = new Hospital();

            $newHospital
                ->setName($hospital[0])
                ->setAddress($hospital[1])
                ->setNpa($hospital[2])
                ->setCity($hospital[3])
                ->setCountry($hospital[4]);

            $manager->persist($newHospital);
        }

        $manager->flush();
    }
}
